import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt('data.csv', delimiter=",", dtype=float)

heights_temp = data[:, 1]
weights_temp = data[:, 2]

heights = np.delete(heights_temp, 0)
weights = np.delete(weights_temp, 0)

# subtask a)
people_count = len(heights)
print(f'Measurements have been made on {people_count} people')

# subtask b)

plt.xlabel('Heights')
plt.ylabel('Weights')
plt.title('Height-Weight Scatter Plot')

plt.scatter(heights, weights, marker=".", s=1)
plt.show()

# subtask c)

heights_every_50 = heights[::50]
weights_every_50 = weights[::50]

plt.xlabel('Heights (every50th)')
plt.ylabel('Weights (every50th)')
plt.title('Height-Weight Scatter Plot (every50th)')

plt.scatter(heights_every_50, weights_every_50, marker=".", s=1)
plt.show()

# subtask d)

min_height = np.min(heights)
max_height = np.max(heights)
mean_height = heights.mean()

print(f'Minimum height: {min_height}')
print(f'Maximum height: {max_height}')
print(f'Mean of all heights: {mean_height}')

# subtask e)

index_male = (data[:, 0] == 1)
index_female = (data[:, 0] == 0)

print(f'Minimum height of males: {data[index_male, 1].min()}')
print(f'Maximum height of males: {data[index_male, 1].max()}')
print(f'Mean of all male heights: {data[index_male, 1].mean()}')

print(f'Minimum height of females: {data[index_female, 1].min()}')
print(f'Maximum height of females: {data[index_female, 1].max()}')
print(f'Mean of all female heights: {data[index_female, 1].mean()}')
