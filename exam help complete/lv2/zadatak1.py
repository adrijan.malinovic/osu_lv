import numpy as np
import matplotlib.pyplot as plt

x = np.array([1,2,3,3,1])
y = np.array([1,2,2,1,1])
plt.axis([0,4,0,4]) #x i y raspon 
plt.plot(x,y,'r', linewidth=3, marker="x", markersize=10)
plt.xlabel('x os')
plt.ylabel('y os')
plt.title('Zadatak 1')
plt.show()