def average_words_in_messages(messages):
    word_count_list = []

    for message in messages:
        word_count = len(message)

        word_count_list.append(word_count)

    return sum(word_count_list)/len(word_count_list)


def message_count_ending_in_exclamation_mark(messages):
    count = 0
    for message in messages:
        if str(message[-1]).endswith('!'):
            count += 1

    return count


sms_file = open('SMSSpamCollection.txt')
ham_messages, spam_messages = [], []

for line in sms_file:
    line = line.rstrip()

    sms_type = line.split()[0]
    if sms_type == 'ham':
        words = line.split()
        ham_messages.append(list(words[1:len(words):1]))
    elif sms_type == 'spam':
        words = line.split()
        spam_messages.append(list(words[1:len(words):1]))


sms_file.close()

print(f'Average word count of ham messages: {str(average_words_in_messages(ham_messages))}')
print(f'Average word count of spam messages: {str(average_words_in_messages(spam_messages))}')

print(f'Count of ham messages ending with "!": {str(message_count_ending_in_exclamation_mark(ham_messages))}')
print(f'Count of spam messages ending with "!": {str(message_count_ending_in_exclamation_mark(spam_messages))}')
