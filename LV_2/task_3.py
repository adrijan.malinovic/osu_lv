import numpy as np
import matplotlib.pyplot as plt

img = plt.imread("road.jpg")
img = img[:, :, 0].copy()

print(img.shape)
print(img.dtype)

image_width = img.shape[1]
image_height = img.shape[0]

plt.figure()

plt.imshow(img, cmap="gray")
plt.show()


# subtask a)

plt.imshow(img, cmap="gray", alpha=0.5)
plt.show()

# subtask b)

index_start = (image_width - 1)//4
index_end = (image_width - 1)//2

second_quarter_by_width_img = img[:, index_start:index_end].copy()

plt.imshow(second_quarter_by_width_img, cmap="gray")
plt.show()

# subtask c)

rotated_img = np.rot90(img, k=-1)

plt.imshow(rotated_img, cmap="gray")
plt.show()

# subtask d)

mirrored_img = np.flip(img, axis=1)

plt.imshow(mirrored_img, cmap="gray")
plt.show()
