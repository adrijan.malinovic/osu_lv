# Naziv velicine ˇ Opis
# Make Proizvoda¯ c vozila (Audi, BMW, Ford. . . ) ˇ
# Model Model vozila (A5 quattro, M5 Sedan, . . . )
# Vehicle Class Klasa vozila (SUV: Small, Two-seater, . . . )
# Engine Size (L) Zapremnina motora u litrama
# Cylinders Broj cilindara (4,5,6,. . . )
# Transmission Tip mjenjacaˇ
# Fuel Type Tip goriva
# Fuel Consumption City (L/100km) Gradska potrošnja u litrama na 100 kilometara
# Fuel Consumption Hwy (L/100km) Izvangradska potrošnja u litrama na 100 kilometara
# Fuel Consumption Comb (L/100km) Kombinirana potrošnja (55% gradska vožnja, 45% izvangradska vožnja) u litrama na 100 kilometara
# Fuel Consumption Comb (mpg) Kombinirana potrošnja (55% gradska vožnja, 45% izvangradska vožnja) u miljama po galonu
# CO2 Emissions (g/km) Emisija C02 plinova u gramima po kilometru za kombiniranu vožnju

# Tip mjenjaca Opis
# A Automatic
# AM Automated manual
# AS Automatic with select shift
# AV Continuously variable
# M Manual
# 3 - 10 Number of gears

# Tip goriva Opis
# X Regular gasoline
# Z Premium gasoline
# D Diesel
# E Ethanol (E85)
# N Natural gas
import pandas as pd
data = pd.read_csv('data_C02_emission.csv')
# SUBTASK A)
print('\n\n\nSubtask a)\n')

print(f'DataFrame has {len(data)} measurements')

print('\nInformation regarding types of data in DataFrame:')
data = data.astype({
    'Make': 'category',
    'Model': 'category',
    'Vehicle Class': 'category',
    'Transmission': 'category',
    'Fuel Type': 'category',
})
print(data.info())

print('\n')
# print(len(data[data.isnull()]))
data.dropna(axis=0)
data.drop_duplicates()
data = data.reset_index(drop=True)


# SUBTASK B)
print('\n\n\nSubtask b)\n')
# print(data.max()['Fuel Consumption City (L/100km)'])
print(
    data.sort_values(
        by='Fuel Consumption City (L/100km)', ascending=False
    )[['Make', 'Model', 'Fuel Consumption City (L/100km)']].head(3)
)
print('\n')
print(
    data.sort_values(
        by='Fuel Consumption City (L/100km)', ascending=True
    )[['Make', 'Model', 'Fuel Consumption City (L/100km)']].head(3)
)

# SUBTASK C)
print('\n\n\nSubtask c)\n')
engine_size_in_range_data = data[
    (data['Engine Size (L)'] < 2.5) & (data['Engine Size (L)'] < 3.5)
    ]
print(engine_size_in_range_data['CO2 Emissions (g/km)'].mean())
# SUBTASK D)
print('\n\n\nSubtask d)\n')
audi_data = data[
    (data['Make'] == 'Audi')
]
audi_count = len(audi_data)
print(audi_count)
audi_4_cylinder_data = audi_data[
    (audi_data['Cylinders'] == 4)
]
print(audi_4_cylinder_data['CO2 Emissions (g/km)'].mean())
# SUBTASK E)
print('\n\n\nSubtask e)\n')
even_cylinders_start_at_4_data = data[
    (data['Cylinders'] >= 4) & (data['Cylinders'] % 2 == 0)
]
print(even_cylinders_start_at_4_data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean())
# SUBTASK F)
print('\n\n\nSubtask f)\n')
diesel_vehicles_data = data[
    (data['Fuel Type'] == 'D')
]
regular_gasoline_vehicles_data = data[
    (data['Fuel Type'] == 'X')
]
print(diesel_vehicles_data['Fuel Consumption City (L/100km)'].mean())
print(regular_gasoline_vehicles_data['Fuel Consumption City (L/100km)'].mean())
# SUBTASK G)
print('\n\n\nSubtask g)\n')
# SUBTASK H)
print('\n\n\nSubtask h)\n')
# SUBTASK I)
print('\n\n\nSubtask i)\n')

