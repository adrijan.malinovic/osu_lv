from sklearn.model_selection import train_test_split
from sklearn . preprocessing import MinMaxScaler
from sklearn . preprocessing import OneHotEncoder
import sklearn . linear_model as lm
from sklearn . metrics import mean_squared_error, mean_absolute_error, mean_squared_error, r2_score, max_error
from math import sqrt
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# subtask a)

data = pd.read_csv('data_C02_emission.csv')

X = data[[
    'Engine Size (L)',
    'Cylinders',
    'Fuel Consumption City (L/100km)',
    'Fuel Consumption Hwy (L/100km)',
    'Fuel Consumption Comb (L/100km)',
    'Fuel Consumption Comb (mpg)',
]]
y = data['CO2 Emissions (g/km)']

X_train , X_test , y_train , y_test = train_test_split(X, y, test_size = 0.2, random_state =1)

# subtask b)

plt.scatter(x = X_train['Engine Size (L)'], y = y_train, s = 1, c = 'blue')
plt.scatter(x = X_test['Engine Size (L)'], y = y_test, s = 1, c = 'red')
plt.legend(['Training Data','Testing Data'])
plt.xlabel('Engine Size (L)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()

# subtask c)

scaler = MinMaxScaler()
X_train_n = pd.DataFrame(scaler.fit_transform(X_train), columns = X_train.columns)
X_test_n = pd.DataFrame(scaler.transform(X_test), columns = X_test.columns)

plt.subplot(2,1,1)

plt.hist(X_train['Engine Size (L)'])
plt.title('Data before scaling')
plt.xlabel('Engine Size (L)')
plt.ylabel('CO2 Emissions (g/km)')

plt.subplot(2,1,2)

plt.hist(X_train_n['Engine Size (L)'])
plt.title('Data after scaling')
plt.xlabel('Engine Size (L)')
plt.ylabel('CO2 Emissions (g/km)')

plt.show()

#subtask d)

linear_regression_model = lm.LinearRegression()
linear_regression_model.fit(X_train_n, y_train)
print(f'Linear regression model parametres: {linear_regression_model.coef_}')

#subtask e)
y_test_p = linear_regression_model.predict(X_test_n)

plt.scatter(x = y_test, y = y_test_p, s = 1)
plt.xlabel('Real values')
plt.ylabel('Predicted values')
plt.show()


#subtask f)
print(f'Mean squared error (MSE): {mean_squared_error(y_test, y_test_p)}')
print(f'Root mean squared error (RMSE): {sqrt(mean_squared_error(y_test, y_test_p))}')
print(f'Mean absolute error (MAE): {mean_absolute_error(y_test, y_test_p)}')
print(f'R^2 score: {r2_score(y_test, y_test_p)}')
