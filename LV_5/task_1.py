import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split

from matplotlib.colors import ListedColormap
from sklearn.linear_model import LogisticRegression

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import \
    confusion_matrix, accuracy_score, precision_score, recall_score, f1_score, ConfusionMatrixDisplay


X, y = make_classification(
    n_samples=200, n_features=2, n_redundant=0, n_informative=2, random_state=213, n_clusters_per_class=1, class_sep=1
)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)


# subtask a)
# class 0 - orange, class 1 - blue, marker . - (training data)
# class 0 - orange, class 1 - blue, marker X - (testing data)

color_map = ListedColormap(['orange', 'blue'])

plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=color_map, marker='.', label='Train')
plt.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=color_map, marker='x', label='Test')

# skuziti kako legend pametnije napraviti
plt.legend()
plt.show()

# subtask b)

model = LogisticRegression()
model.fit(X_train, y_train)

# subtask c)
theta0 = model.intercept_[0]
theta1, theta2 = model.coef_.T

# granica odluke  theta0 + theta1 * x1 + theta2 * x2 = 0
a = -theta0 / theta2
b = -theta1 / theta2

x_min, x_max = -4, 4
y_min, y_max = -4, 4

xd = np.array([x_min, x_max])
yd = b * xd + a

plt.scatter(x=X_train[:, 0], y=X_train[:, 1], c=y_train, cmap=color_map, label='Train')
plt.plot(xd, yd, 'k', lw=1, ls='--')
plt.fill_between(xd, yd, y_min, color='tab:blue', alpha=0.2)
plt.fill_between(xd, yd, y_max, color='tab:red', alpha=0.2)
plt.xlim(x_min, x_max)
plt.ylim(y_min, y_max)
plt.show()

# subtask d)
y_test_p = model.predict(X_test)
cm = confusion_matrix(y_test, y_test_p)
disp = ConfusionMatrixDisplay(cm)
disp.plot()
plt.show()
print(f'Accuracy: {accuracy_score(y_test, y_test_p)}')
print(f'Precision: {precision_score(y_test, y_test_p)}')
print(f'Recall: {recall_score(y_test, y_test_p)}')
print(f'F1 score: {f1_score(y_test, y_test_p)}')

# subtask e)
boolArray = y_test == y_test_p
cmap = ListedColormap(['black', 'green'])
plt.scatter(x=X_test[:, 0], y=X_test[:, 1], c=boolArray, cmap=cmap)
plt.colorbar(label='False/True', ticks=np.linspace(0, 1, 2))
plt.show()


