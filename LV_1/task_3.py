def average_value(lst: list):
    return sum(lst) / len(lst)


numbers = []

while True:
    string = input('Enter number or "Done": ')
    if string == 'Done':
        break

    try:
        n = float(string)

    except ValueError:
        print('The input value is not a number...')
        continue

    numbers.append(n)

print(f'You have entered {len(numbers)} numbers.')
print(f'Average value: {average_value(numbers)}')
print(f'Minimal value: {min(numbers)}')
print(f'Maximal value: {max(numbers)}')
numbers.sort()
print(f'Sorted list of numbers: {str(numbers)}')
