# import biblioteka
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.neighbors import KNeighborsClassifier
from matplotlib.colors import ListedColormap
from sklearn.metrics import accuracy_score
from sklearn.model_selection import GridSearchCV
from tensorflow import keras
from keras import layers
##################################################
# 1. zadatak
##################################################

# učitavanje dataseta
titanic_data = pd.read_csv('titanic.csv')
# a)
print(titanic_data.describe())  # Vidimo da je count 891, toliko ima entry-ja u dataset-u pa ima toliko i osoba
# b)
total_survived_count = titanic_data.query('Survived==1')['Survived'].count()
print(f'Total Survived Count: {total_survived_count}\n\n')
# c)
titanic_survived_data = titanic_data.query('Survived==1')

male_count = titanic_data.query('Sex=="male"')['Sex'].count()
female_count = titanic_data.query('Sex=="female"')['Sex'].count()

total_count = male_count + female_count

male_survived_count = titanic_survived_data.query('Sex=="male"')['Sex'].count()
female_survived_count = titanic_survived_data.query('Sex=="female"')['Sex'].count()


print(f'Male Count: {male_count}')
print(f'Female Count: {female_count}')
print(f'Total Count: {total_count}\n\n')

print(f'Male Survived Count: {male_survived_count}')
print(f'Female Survived Count: {female_survived_count}')
print(f'Total Survived Count: {total_survived_count}\n\n')
#  moram ovo dovrsit jer nije dobar plot ##############################oznacit#########################################
plt.figure()
plt.bar(
    data=[male_survived_count, female_survived_count],
    height=total_survived_count,
    x=[0, 1]
)
plt.show()
# d)
titanic_survived_male_data = titanic_survived_data.query('Sex=="male"')
titanic_survived_female_data = titanic_survived_data.query('Sex=="female"')

print('Survived Male Data - Age Description')
print(titanic_survived_male_data['Age'].describe())
print('\n\n')

print('Survived Female Data - Age Description')
print(titanic_survived_female_data['Age'].describe())
print('\n\n')
# e)
titanic_survived_male_class_1_data = titanic_survived_male_data.query('Pclass==1')
titanic_survived_male_class_2_data = titanic_survived_male_data.query('Pclass==2')
titanic_survived_male_class_3_data = titanic_survived_male_data.query('Pclass==3')

print(f"Survived Males - Class 1 Age Description\n{titanic_survived_male_class_1_data['Age'].describe()}\n")
print(f"Survived Males - Class 2 Age Description\n{titanic_survived_male_class_2_data['Age'].describe()}\n")
print(f"Survived Males - Class 3 Age Description\n{titanic_survived_male_class_3_data['Age'].describe()}\n")
# ovdje gledamo minimume od description-a od Age-a za svaku klasu


##################################################
# 2. zadatak
##################################################

# učitavanje dataseta
titanic_data = pd.read_csv('titanic.csv')

titanic_model_data = titanic_data[['Pclass', 'Sex', 'Fare', 'Embarked', 'Survived']]
titanic_model_data = titanic_model_data.dropna(axis=0)

# train test split

X = titanic_model_data[['Pclass', 'Sex', 'Fare', 'Embarked']].to_numpy()
y = titanic_model_data['Survived'].to_numpy()

ohe = OneHotEncoder()
X = ohe.fit_transform(X)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, stratify=y, random_state=10)

# skaliraj ulazne velicine
sc = StandardScaler(with_mean=False)


X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform(X_test)
# a)
"""
 Izradite algoritam KNN na skupu podataka za ucenje (uz K=5). Vizualizirajte podatkovne ˇ
primjere i granicu odluke.
"""


def plot_decision_regions(X, y, classifier, resolution=0.02):
    plt.figure()
    # setup marker generator and color map
    markers = ('s', 'x', 'o', '^', 'v')
    colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])

    # plot the decision surface
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution), np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.3, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())

    # plot class examples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y == cl, 0],
                    y=X[y == cl, 1],
                    alpha=0.8,
                    c=colors[idx],
                    marker=markers[idx],
                    label=cl)


knn_5_model = KNeighborsClassifier(n_neighbors=5)
knn_5_model.fit(X_train_n, y_train)

"""
plot_decision_regions(X_train_n, y_train, classifier=knn_5_model)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("N=5")
plt.tight_layout()
plt.show()
"""

y_train_predicted_knn_5 = knn_5_model.predict(X_train_n)
y_test_predicted_knn_5 = knn_5_model.predict(X_test_n)
# b)

print("KNN 5: ")
print("Accuracy train: " + "{:0.3f}".format((accuracy_score(y_train, y_train_predicted_knn_5))))
print("Accuracy test: " + "{:0.3f}".format((accuracy_score(y_test, y_test_predicted_knn_5))))
# c)
"""
 Pomocu unakrsne validacije odredite optimalnu vrijednost hiperparametra K algoritma ´
KNN.
"""
knn_model_unknown_k = KNeighborsClassifier()

param_grid = {
    'n_neighbors': [
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
    ]
}

knn_gscv = GridSearchCV(
    knn_model_unknown_k,
    param_grid,
    cv=5,
    scoring='accuracy',
    n_jobs=-1
)

knn_gscv.fit(X_train_n, y_train)
print(f'Best Params:\n\n{knn_gscv.best_params_}\n\n')
print(f'Best Score:\n{knn_gscv.best_score_}\n\n')
# Vidimo da je najbolji K = 13, a najbolji accuracy 0.7691588785046729

#d)
"""
Izracunajte to ˇ cnost klasi ˇ fikacije na skupu podataka za ucenje i skupu podataka za testiranje ˇ
za dobiveni K. Usporedite dobivene rezultate s rezultatima kada je K=5
"""
knn_13_model = KNeighborsClassifier(n_neighbors=13)
knn_13_model.fit(X_train_n, y_train)

y_train_predicted_knn_13 = knn_13_model.predict(X_train_n)
y_test_predicted_knn_13 = knn_13_model.predict(X_test_n)


print("KNN 13: ")
print("Accuracy train: " + "{:0.3f}".format((accuracy_score(y_train, y_train_predicted_knn_13))))
print("Accuracy test: " + "{:0.3f}".format((accuracy_score(y_test, y_test_predicted_knn_13))))
##################################################
# 3. zadatak
##################################################

# učitavanje podataka:
# isto kao gore, samo se test size promjenio, pa nisam duplicirao isti kod
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, stratify=y, random_state=10)
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform(X_test)

# a)
"""
Izgradite neuronsku mrežu sa sljedecim karakteristikama: ´
- model ocekuje ulazne podatke X
- prvi skriveni sloj, 16 neurona, relu 
- drugi skriveni sloj, 8 neurona, relu 
- treci skriveni sloj, 4 neurona, relu
- izlazni sloj, 1 neuron, sigmoid

Ispišite informacije o mreži u terminal.
"""
model = keras.Sequential()

model.add(
    layers.Input(shape=(19, ))
)
model.add(
    layers.Dense(16, activation='relu')
)
model.add(
    layers.Dense(8, activation='relu')
)
model.add(
    layers.Dense(4, activation='relu')
)
model.add(
    layers.Dense(1, activation='sigmoid')
)

print('\n\n')
model.summary()
#b)

#c)

#d)

#e)

#f)