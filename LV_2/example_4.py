import numpy as np
import matplotlib.pyplot as plt
x = np . linspace(0, 6, num=30)
y = np . sin(x)
plt.plot(x, y, 'r', linewidth=3, marker=".", markersize=10)
plt.axis([0, 6, -2, 2])
plt.xlabel('X - axis')
plt.ylabel('y = sin(x)')
plt.title('sinus function')
plt.show()
