try:
    score = float(input('Enter your score: '))

    if score < 0.0 or score > 1.0:
        print('The score must be between 0.0 and 1.0')
    else:
        if score >= 0.9:
            print('Category: A')
        elif score >= 0.8:
            print('Category: B')
        elif score >= 0.7:
            print('Category: C')
        elif score >= 0.6:
            print('Category: D')
        else:
            print('Category: D')

except ValueError:
    print('The input value is not a valid score...')
