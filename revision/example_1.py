import numpy as np
import matplotlib.pyplot as plt

# Define the number of data samples
data_count = 1000

# Generate random data between -1 and 1, with a third column for color gradient
data_uniform = np.concatenate(
    [
        # Range -1 to 1
        np.random.uniform(-1, 1, size=(data_count, 2)),
        # Range 0 to 1
        np.random.rand(data_count, 1)
    ],
    # Concatenate column-wise
    axis=1
)

# Generate another set of random data
data_normal = np.concatenate(
    [
        np.random.normal(loc=0, scale=0.3, size=(data_count, 2)),
        np.random.rand(data_count, 1)
    ],
    axis=1
)

# Stack the two data sets along the first dimension to create a 3D array
data = np.stack((data_uniform, data_normal), axis=0)

# Create a 2x2 grid of subplots
fig, axs = plt.subplots(2, 2)

# Create the scatter plot
scat = axs[0, 0].scatter(
    data_uniform[:, 0],  # First column - X coordinate
    data_uniform[:, 1],  # Second column - Y coordinate

    c=data_uniform[:, 2],  # Third column - colormap gradient
    cmap='viridis',  # Use the 'viridis' colormap

    s=10  # Size of points
)
fig.colorbar(scat, ax=axs[0, 0])

# Create a second scatter plot
scat = axs[0, 1].scatter(
    data_normal[:, 0],  # First column - X coordinate
    data_normal[:, 1],  # Second column - Y coordinate

    c=data_normal[:, 2],  # Third column - colormap gradient
    cmap='viridis',  # Use the 'viridis' colormap

    s=10  # Size of points
)
fig.colorbar(scat, ax=axs[0, 1])

# Set the limits of the x-axis and y-axis to always display from -1 to 1
axs[0, 0].set_xlim(-1, 1)
axs[0, 0].set_ylim(-1, 1)

axs[0, 1].set_xlim(-1, 1)
axs[0, 1].set_ylim(-1, 1)

# Add a color bar and axis labels and a title
axs[0, 0].set_xlabel('X')
axs[0, 0].set_ylabel('Y')
axs[0, 0].set_title('Uniform')

axs[0, 1].set_xlabel('X')
axs[0, 1].set_ylabel('Y')
axs[0, 1].set_title('Normal')

# Show the plot
plt.show()


