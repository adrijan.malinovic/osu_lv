import numpy as np
import matplotlib.pyplot as plt

white_square_50 = np.full([50, 50], fill_value=255, dtype=int)
black_square_50 = np.zeros([50, 50], dtype=int)

print(white_square_50)

plt.imshow(white_square_50, cmap="gray", vmin=0, vmax=255)
plt.show()
