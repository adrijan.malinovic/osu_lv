import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("imgs\\test_4.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w, h, d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

# subtask 1
color_number_original = len(np.unique(img_array_aprox, axis=0))
print(f'Number of unique colors in the original image: {color_number_original}')

# subtask 2
color_clusters = 2
k_means_model = KMeans(n_clusters=color_clusters)

k_means_model.fit(img_array_aprox)
labels = k_means_model.predict(img_array_aprox)

new_colors = k_means_model.cluster_centers_[labels]

# subtask 3

new_image = np.reshape(new_colors, (w, h, d))

plt.figure()
plt.title('New Image')
plt.imshow(new_image)
plt.tight_layout()
plt.show()

# subtask 4 - mijenjati K
# subtask 5 - mijenjati slike

# subtask 6
labels_initial = labels
j = []
for n_clusters in range(1, 10):
    k_means_model = KMeans(n_clusters=n_clusters, n_init=5)
    k_means_model.fit(img_array_aprox)
    labels = k_means_model.predict(img_array_aprox)
    j.append(k_means_model.inertia_)

plt.plot(range(1, 10), j)
plt.xlabel('K')
plt.ylabel('J')
plt.show()

# subtask 7
"""
Elemente slike koji pripadaju jednoj grupi prikažite kao zasebnu binarnu sliku. 
Što primjecujete?
"""
for i in range(color_clusters):
    bool_array = labels_initial == i
    bool_image = np.reshape(bool_array, (w, h))
    plt.subplot(1, color_clusters, i + 1)
    plt.title(f"{i + 1}. image")
    plt.imshow(bool_image)

plt.show()

