import numpy as np
from tensorflow import keras
from keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

# TODO: prikazi nekoliko slika iz train skupa

for i in range(5):
    plt.subplot(1, 5, i+1)
    rand_index = np.random.randint(low=0, high=28 * 28 - 1, size=None, dtype=int)
    plt.imshow(

        x_train[rand_index],
        cmap='gray'
    )
    plt.title(
        f"{y_train[rand_index]}"
    )
plt.show()

# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)

#reshape

x_train_s = np.reshape(x_train_s, (60000, 784))
x_test_s = np.reshape(x_test_s, (10000, 784))


# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu

model = keras.Sequential()

model.add(
    layers.Input(shape=(784, ))
)
model.add(
    layers.Dense(100, activation='relu')
)
model.add(
    layers.Dense(50, activation='relu')
)
model.add(
    layers.Dense(10, activation='softmax')
)

print('\n\n')
model.summary()

# TODO: definiraj karakteristike procesa ucenja pomocu .compile()

model.compile(
    loss="categorical_crossentropy",
    optimizer="adam",
    metrics=["accuracy", ]
)

# TODO: provedi ucenje mreze

history = model.fit(
    x_train_s,
    y_train_s,
    batch_size=32,
    epochs=20,
    validation_split=0.1
)

predictions = model.predict(x_test_s)

# TODO: Prikazi test accuracy i matricu zabune

score = model.evaluate(x_test_s, y_test_s, verbose=1)

print('\n\n')
print(score)

y_test_predicted = np.argmax(predictions, axis=1)

conf_mat = confusion_matrix(y_test, y_test_predicted)

display = ConfusionMatrixDisplay(conf_mat)
display.plot()
plt.show()

# TODO: spremi model

model.save("FCN/")
