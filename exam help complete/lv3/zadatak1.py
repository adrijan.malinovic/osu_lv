import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_csv('data_C02_emission.csv')

MAKE = 'Make'
MODEL = 'Model'
VEHCILE_CLASS = 'Vehicle Class'
ENGINE_SIZE = 'Engine Size (L)'
CYLINDERS = 'Cylinders'
TRANSIMSSION = 'Transmission'
FUEL_TYPE = 'Fuel Type'
FUEL_CONSUMPTION_CITY = 'Fuel Consumption City (L/100km)'
FUEL_CONSUMPTION_HWY = 'Fuel Consumption Hwy (L/100km)'
FUEL_CONSUMPTION_COMB_L = 'Fuel Consumption Comb (L/100km)'
FUEL_CONSUMPTION_COMB_MPG = 'Fuel Consumption Comb (mpg)'
CO2_EMISSIONS = 'CO2 Emissions (g/km)'

# a)
# Koliko mjerenja sadrži DataFrame? Kojeg je tipa svaka velicina? Postoje li izostale ili
# duplicirane vrijednosti? Obrišite ih ako postoje. Kategoricke velicine konvertirajte u tip
# category.

sample_size = data.shape[0]
print("Broj mjerenja: " + str(sample_size))

if data.duplicated().sum() > 0:
    data.drop_duplicates()
else:
    print("Nema dupliciranih vrijednosti!")

data[MAKE] = data[MAKE].astype('category')
data[MODEL] = data[MODEL].astype('category')
data[VEHCILE_CLASS] = data[VEHCILE_CLASS].astype('category')
data[TRANSIMSSION] = data[TRANSIMSSION].astype('category')
data[FUEL_TYPE] = data[FUEL_TYPE].astype('category')

print(data.info())

# b)
# Koja tri automobila ima najvecu odnosno najmanju gradsku potrošnju? Ispišite u terminal:
# ime proizvodaca, model vozila i kolika je gradska potrošnja.

sorted_by_city_fuel_consumption = data.sort_values(FUEL_CONSUMPTION_CITY, ascending=False)

print("Auti koji troše najviše: ")
print(sorted_by_city_fuel_consumption.head(3)[[MAKE,MODEL,FUEL_CONSUMPTION_CITY]])
print("\nAuti koji troše najmanji: ")
print(sorted_by_city_fuel_consumption.tail(3)[[MAKE,MODEL,FUEL_CONSUMPTION_CITY]])

# c)
# Koliko vozila ima velicinu motora izmedu 2.5 i 3.5 L? Kolika je prosjecna C02 emisija
# plinova za ova vozila?

target_cars = data[(data[ENGINE_SIZE] >= 2.5) & (data[ENGINE_SIZE] <= 3.5)]
print("\nBroj auto s motorom od [2.5,3.5] L: " + str(target_cars.shape[0]))
print("Prosjecana C02 emisija: " + str(target_cars[CO2_EMISSIONS].mean()))

# d)
# Koliko mjerenja se odnosi na vozila proizvodaca Audi? Kolika je prosjecna emisija C02
# plinova automobila proizvodaca Audi koji imaju 4 cilindara?

make = 'Audi'
cylinders = 4

audi_cars = data[(data[MAKE] == make) & (data[CYLINDERS] == cylinders)]
print("\nBroj audi auta: " + str(audi_cars.shape[0]))
print("Prosjecna CO2 emisija: " + str(audi_cars[CO2_EMISSIONS].mean()))

# e)
# Koliko je vozila s 4,6,8... cilindara? Kolika je prosjecna emisija C02 plinova s obzirom na
# broj cilindara?

cars_grouped_by_cylinders = data.groupby(CYLINDERS).agg(Average_CO2_Emission = (CO2_EMISSIONS,'mean'))
print(cars_grouped_by_cylinders)
print()
# f)
# Kolika je prosjecna gradska potrošnja u slucaju vozila koja koriste dizel, a kolika za vozila
# koja koriste regularni benzin? Koliko iznose medijalne vrijednosti?

grouped = data.groupby(FUEL_TYPE).agg(  Fuel_consumption_city = (FUEL_CONSUMPTION_CITY,'median'),
                                        Fuel_consumption_Hwy = (FUEL_CONSUMPTION_HWY,'median'),
                                        Fuel_consumption_Comp_L = (FUEL_CONSUMPTION_COMB_L,'median'),
                                        Fuel_consumption_Comb_mpg = (FUEL_CONSUMPTION_COMB_MPG,'median'),
                                        CO2_Emissions = (CO2_EMISSIONS,'median')
                                    )
print(grouped.iloc[[0,2]])

# g)
# Koje vozilo s 4 cilindra koje koristi dizelski motor ima najvecu gradsku potrošnju goriva?

target_car = data[(data[CYLINDERS] == 4) & (data[FUEL_TYPE]=='D')].sort_values(FUEL_CONSUMPTION_CITY,ascending=False)
print()
print(target_car.head(1)[[MAKE,MODEL,FUEL_CONSUMPTION_CITY]])

# h)
# Koliko ima vozila ima rucni tip mjenjaca (bez obzira na broj brzina)?

manual_cars = data[data[TRANSIMSSION].str.startswith('M')]
print("\nBroj auta s ručnim mjenjacem: " + str(manual_cars.shape[0]))

# i)
# Izracunajte korelaciju izmedu numerickih velicina. Komentirajte dobiveni rezultat.

print(data.corr()[[FUEL_CONSUMPTION_CITY,FUEL_CONSUMPTION_HWY,FUEL_CONSUMPTION_COMB_MPG,FUEL_CONSUMPTION_COMB_L,CO2_EMISSIONS]])
