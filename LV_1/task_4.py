def words_to_dictionary(lst: list):
    dictionary = {}
    for word in lst:
        if word not in dictionary.keys():
            dictionary[word] = 0
            for other_word in lst:
                if word == other_word:
                    dictionary[word] += 1
    return dictionary


song_file = open('song.txt')
words = []
for line in song_file:
    line = line.rstrip()
    words += line.split()
song_file.close()

unique_words = []
song_dictionary = words_to_dictionary(words)

for wrd, count in song_dictionary.items():
    if count == 1:
        unique_words.append(wrd)

print('These words appear only once in the given song: ')
print(str(unique_words))
print(f'There are {len(unique_words)} of them.')
