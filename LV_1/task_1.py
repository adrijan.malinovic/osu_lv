def total_euro(hrs: float, rph: float):
    return hrs * rph


while True:
    try:
        hours = float(input('Enter your work hours: '))
        rate_per_hour = float(input('Enter your rate per hour: '))
        break
    except ValueError:
        print('The input value is not a number...')

print(f'Your expected paycheck: {total_euro(hours, rate_per_hour)} euro')
